package com.szkolenieandroid.contentproviderexample;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class ContactProviderExampleActivity extends Activity implements  LoaderManager.LoaderCallbacks<Cursor> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_provider_example);
        getLoaderManager().initLoader(0, null, this);
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};

        String select = ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1";
        return new CursorLoader(this, ContactsContract.Contacts.CONTENT_URI,
                projection, select, null,
                ContactsContract.Contacts.DISPLAY_NAME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor contactsCursor) {

        StringBuffer output = new StringBuffer();
        if (contactsCursor.getCount() > 0) {

            while (contactsCursor.moveToNext()) {

                String name = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String contactId = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts._ID));
                output.append("\n First Name:" + name);

                loadPhoneNumbersForContact(output, contactId);

            }
        }
        TextView outputContent = (TextView) findViewById(R.id.outputText);
        outputContent.setText(output.toString());
    }

    private void loadPhoneNumbersForContact(StringBuffer output, String contactId) {
        Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{contactId}, null);

        while (phoneCursor.moveToNext()) {
            String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            output.append("\n Phone number:" + phoneNumber);
        }

        phoneCursor.close();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
